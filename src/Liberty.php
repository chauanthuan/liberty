<?php
namespace Dayone\Issuer;

class Liberty {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {

        \App::register('Dayone\Issuer\LibertyServiceProvider');
        return 'Liberty::index';
    }

}